package com.nacos.consumer.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * @author 邓向军
 * @date2021/4/25 19:08
 */
@Configuration
public class RestTemplateConfig {
    @Bean
    public RestTemplate getResTemplate()
    {
        return new RestTemplate();
    }
}
